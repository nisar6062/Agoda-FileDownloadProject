
# File Downloader

This Application helps in downloading a file from a URL or a FTP location.

For URL - you need 
  - remote URL
  - local file path where file needs to be saved.

For FTP - you need 
  - server name
  - server usename/password
  - remote URL
  - local file path where file needs to be saved.


How to Run

  - Run main method in FileDownloader class
  - For a list of url input use fileDownloadFromListOfURLs method in FileDownloader class
