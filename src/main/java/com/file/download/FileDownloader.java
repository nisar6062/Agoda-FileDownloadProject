package com.file.download;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.net.ftp.FTPClient;

public class FileDownloader {
    private final static Logger LOGGER = Logger.getLogger(FileDownloader.class.getName());
    private static final int FTP_TIME_OUT = 30000;
    private static String localFilePath = null;
    
    public FileDownloader() {
        localFilePath = PropertyFileReader.readProperty("localFilePath");
    }
    /*
     * Newly added for list of urls ftp://username:password@hostname:port/file
     * http://file
     */
    public void fileDownloadFromListOfURLs(List<String> urlList) {
        urlList.forEach(url -> {
            if (url.indexOf("http") != -1) {
                try {
                    downloadFromURL(localFilePath, url);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (url.indexOf("ftp") != -1) {
                String val[] = url.split("ftp://")[1].split("@");
                String userPasss[] = val[0].split(":");
                String hostPort[] = val[1].split(":");
                String portFile[] = hostPort[1].split("/");
                downloadFromFTP(hostPort[0], userPasss[0], userPasss[1], portFile[1], localFilePath + portFile[1],
                        Integer.valueOf(portFile[0]));
            }
        });
    }

    public static void main(String[] args) throws IOException {
        FileDownloader downladerObj = new FileDownloader();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Type 0 for Download by URL or Type 1 for Download by FTP: ");
            String input = br.readLine();
            if ("0".equals(input)) {
                try {
                    System.out.print("Enter URL to be downloaded: ");
                    String url = br.readLine();
                    System.out.print("Enter localFilePath: ");
                    String localFilePath = br.readLine();
                    downladerObj.downloadFromURL(localFilePath, url);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if ("1".equals(input)) {
                System.out.print("Enter server name: ");
                String serverName = br.readLine();
                System.out.print("Enter server usename: ");
                String usename = br.readLine();
                System.out.print("Enter server password: ");
                String password = br.readLine();
                System.out.print("Enter remote file: ");
                String remoteFile = br.readLine();
                System.out.print("Enter local File location: ");
                String localFile = br.readLine();
                System.out.print("Enter port number: ");
                int port = Integer.parseInt(br.readLine());
                downladerObj.downloadFromFTP(serverName, usename, password, remoteFile, localFile, port);
            } else {
                System.out.println("Invalid Request");
            }
            System.out.println("input : " + input);
            System.out.println("-----------\n");
        } catch (IOException e) {
            System.out.println("Error in the Request");
            LOGGER.log(Level.SEVERE, e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    System.out.println("Error in the Request");
                    LOGGER.log(Level.SEVERE, e.getMessage());
                }
            }
        }
    }

    public boolean downloadFromURL(final String localFilePath, final String urlString) throws IOException {
        BufferedInputStream in = null;
        FileOutputStream fout = null;
        try {
            in = new BufferedInputStream(new URL(urlString).openStream());
            fout = new FileOutputStream(localFilePath);
            final byte data[] = new byte[1024];
            int count;
            while ((count = in.read(data, 0, 1024)) != -1) {
                fout.write(data, 0, count);
            }
        } catch (Exception e) {
            System.out.println("Error in the Request");
            LOGGER.log(Level.SEVERE, e.getMessage());
            new File(localFilePath).delete();
            return false;
        } finally {
            if (in != null) {
                in.close();
            }
            if (fout != null) {
                fout.close();
            }
        }
        return true;
    }

    public boolean downloadFromFTP(String serverAddress, String username, String password, String remoteFilePath,
            String localFilePath, int port) {
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.setDefaultTimeout(FTP_TIME_OUT);
            ftpClient.connect(serverAddress, port);
            ftpClient.login(username, password);
            ftpClient.enterLocalPassiveMode();
            File localfile = new File(localFilePath);
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(localfile));
            boolean success = ftpClient.retrieveFile(remoteFilePath, outputStream);
            outputStream.close();
            System.out.println("Success: " + success);
            if (success) {
                System.out.println("Ftp file successfully downloaded.");
            }
        } catch (Exception ex) {
            System.out.println("Error occurs in downloading files from ftp Server : " + ex.getMessage());
            new File(localFilePath).delete();
            return false;
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                return false;
            }
        }
        return true;
    }
}